import Home from "./views/Home";
import Details from "./views/Details";
import Search from "./views/Search";

export default [
  { path: "/search/:name", component: Search, exact: true },
  { path: "/details/:id", component: Details, exact: true },
  { path: "", component: Home, exact: true }
];
