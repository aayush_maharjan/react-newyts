import React, { Component } from "react";
import axios from "axios";
import MovieCard from "../components/MovieCard/MovieCard";
class Search extends Component {
  state = {
    movieParam: "",
    movies: []
  };
  componentDidMount() {
    console.log(this.props);
    this.setState({
      movieParam: this.props.match.params.name
    });
    axios
      .get(
        "https://yts.lt/api/v2/list_movies.json?query_term=" +
          this.props.match.params.name
      )
      .then(res => {
        console.log(res.data.data.movies);
        this.setState({
          movies: res.data.data.movies
        });
      });
  }
  render() {
    return (
      <div className={"main-body"}>
        <h1>{this.state.movieParam}</h1>
        <div className={"content"}>
          <div className="movie-list">
            {this.state.movies.map((movie, key) => (
              <MovieCard movie={movie} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Search;
