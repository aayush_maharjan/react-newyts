import React, { Component } from "react";
import axios from "axios";
class Movie extends Component {
  state = {
    movie: {}
  };

  componentDidMount() {
    axios
      .get(
        "https://yts.lt/api/v2/list_movies.json?query_term=" +
          this.props.match.params.name
      )
      .then(res => {
        console.log(res.data.data);
        this.setState({
          movie: res.data.data.movie
        });
      });
  }
  render() {
    return <div>{this.state.movie.title}</div>;
  }
}

export default Movie;
