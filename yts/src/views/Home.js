import React, { Component } from "react";
import MovieCard from "../components/MovieCard/MovieCard";
import pic from "../assets/images/pic.png";
import axios from "axios";
class Home extends Component {
  state = {
    movies: [
      { id: 1, title: "Ram G", img: pic },
      { id: 2, title: "Ram G Returns", img: pic },
      { id: 3, title: "Ram G Again", img: pic }
    ],
    search: ""
  };

  componentDidMount() {
    axios.get("https://yts.lt/api/v2/list_movies.json").then(res => {
      console.log(res.data.data.movies);
      this.setState({
        movies: res.data.data.movies
      });
    });
  }

  inputHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  goToSearch = () => {
    this.props.history.push("/search/" + this.state.search);
  };

  render() {
    return (
      <>
        <div className="search-box">
          <input
            type="text"
            name={"search"}
            onChange={e => this.inputHandler(e)}
          />
          <button onClick={this.goToSearch}>Search</button>
        </div>
        <div className={"content"}>
          <div className="movie-list">
            {this.state.movies.map((movie, key) => (
              <MovieCard movie={movie} />
            ))}
          </div>
        </div>
      </>
    );
  }
}

export default Home;
