import React, { Component } from "react";
import NavBar from "../../containers/Navbar/NavBar";
import { Route, Switch } from "react-router-dom";
import routes from "../../AppRoutes";
import pic from "../../assets/images/pic.png";

class Layout extends Component {
  state = {
    navItems: [
      { id: 1, label: "Home", path: "" },
      { id: 2, label: "Search", path: "" },
      { id: 2, label: "Details", path: "" }
    ]
  };
  render() {
    return (
      <section>
        <NavBar navItems={this.state.navItems} />
        <div className={"content-area"}>
          <Switch>
            {routes.map((route, key) => (
              <Route
                path={route.path}
                component={route.component}
                exact={route.exact}
              />
            ))}
          </Switch>
        </div>
      </section>
    );
  }
}

export default Layout;
