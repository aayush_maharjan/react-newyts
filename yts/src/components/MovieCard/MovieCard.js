import React from 'react';
import {Link} from "react-router-dom";

const MovieCard= props=>(
    <div className="movie-card">
        <img src={props.movie.large_cover_image} alt=""/>
        <Link to={`/details/${props.movie.id}`}><h4>{props.movie.title}</h4></Link>
    </div>
)

export default MovieCard;