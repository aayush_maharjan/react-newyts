import React, { Component } from "react";

class LabRat2 extends Component {
  state = {
    name: "",
    email: "",
    age: ""
  };
  inputHandler = e => {
    console.log(e.target.name);
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  render() {
    return (
      <div>
        <input
          style={MyStyle}
          type="text"
          onChange={e => this.inputHandler(e)}
          placeholder={"enter a name"}
          name={"name"}
        />
        <input
          style={MyStyle}
          type="text"
          onChange={e => this.inputHandler(e)}
          placeholder={"enter a email"}
          name={"email"}
        />
        <input
          style={MyStyle}
          type="text"
          onChange={e => this.inputHandler(e)}
          placeholder={"enter a age"}
          name={"age"}
        />
        <div>
          <h1>
            Name: {this.state.name}
            <br />
            Email: {this.state.email}
            <br />
            Age: {this.state.age}
            <br />
          </h1>
        </div>
      </div>
    );
  }
}

const MyStyle = {
  margin: "16px",
  height: "5vh",
  padding: "16px",
  border: "1px solid black"
};
export default LabRat2;
